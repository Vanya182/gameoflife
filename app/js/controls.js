require("jquery");
require("ion-rangeslider");

export class Controls{
	constructor(app){
		this.app = app;
		this.paused = 0;
		this.controls = {
			populateRandom: document.getElementsByClassName("js-populate-random")[0],
			loadPattern: document.getElementsByClassName("js-load-pattern"),
			run: document.getElementsByClassName("js-run")[0],
			pause: document.getElementsByClassName("js-pause-tick")[0],
			stop: document.getElementsByClassName("js-clear-tick")[0],
			tickIntervalPicker: jQuery(".js-generation-speed-picker")
		};
		this.controlsListeners();
	}

	controlsListeners(){
		this.controls.populateRandom.addEventListener("click", () => {
			this.app.clearTick();
			this.app.populateRandom();
		});

		Array.prototype.map.call(this.controls.loadPattern, control => {
			control.addEventListener("click", (e) => {
				this.app.clearTick();
				let patternName = event.target.dataset.pattern;
				this.app.loadPattern(patternName);
			});
		});
		

		this.controls.run.addEventListener("click", () => {
			this.app.initTick();
		});

		this.controls.pause.addEventListener("click", () => {
			if (this.app.isAppRunning()){
				this.app.pauseTick();
				this.controls.pause.text = this.paused ? "pause" : "resume";
				this.paused = !this.paused;
			}
		});

		this.controls.stop.addEventListener("click", () => {
			this.app.clearTick();
			this.controls.pause.text = "pause";
			this.app.tickInterval = 0;
			let slider = this.controls.tickIntervalPicker.data("ionRangeSlider");
			slider.update({
			    from: 0
			});
		});

		this.controls.tickIntervalPicker.ionRangeSlider({
		 	type: "single",
		    min: 0,
		    max: 1500,
		    step: 100,
		    onChange: data => {
        		let generationStep = data.from;
				this.app.attachTickIntervalToTicker(generationStep);
			},
		})
	}
};