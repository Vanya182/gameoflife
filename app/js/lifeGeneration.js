export class LifeGeneration{
	constructor(state){
		this.state = state;
	}
	run(){
		let arrayDimension = this.state.length;
	  	let state = this.state;
		let newState = [];

		for (let i = 0; i < arrayDimension; i++) {
		  for (let j = 0; j < arrayDimension; j++) {
	  		let cellNeighbors = 0;
			/**
			 * Loop through temporary cell neighbors
			 */
			let cellY = i;
			let cellX = j;
	  		for( let k = cellY - 1; k <= cellY + 1; k++) {
            	for(let l =  cellX -1;  l <= cellX + 1; l++) {
	            	/**
	            	 * Check for not going out of array range
	            	 */
	                if(
	                	! (k == cellY && l == cellX) &&
	            		(k >= 0 && k < arrayDimension && l >= 0 && l < arrayDimension)
	            	){
	             		cellNeighbors += state[k][l];
	                }
	                
	            }
	        }
	        /**
	         * If cell is dead at the moment
	         */
	        if (! state[i][j]){
	        	if (cellNeighbors == 3){
	            	newState.push({
	            		"x": i,
	            		"y": j,
	            		"value": true
	            	});
	        	}
	        }else{
	    	/**
	         * If cell is alive at the moment
	         */
	     	 	if (cellNeighbors <= 1 || cellNeighbors >= 4){
	        		newState.push({
	            		"x": i,
	            		"y": j,
	            		"value": false
	            	});
	            }
	            if (cellNeighbors == 2 || cellNeighbors == 3){
	            	newState.push({
	            		"x": i,
	            		"y": j,
	            		"value": true
	            	});
	            }
	        }
		  }
		}

		newState.map(item=>{
			this.state[item.x][item.y] = item.value;
		})
		return this.state;
	}
};