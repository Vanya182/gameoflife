import {LifeGeneration} from "./lifeGeneration";
import {Controls} from "./controls";
import {patterns} from "./patterns";
window.jQuery = require("jquery");
require("bootstrap");

class App {
	constructor(cellsCount = 200, tickInterval = 0){
		/**
		 * 1 -running
		 */
		this.appStatus = 0;
		this.cellsCount = cellsCount;
		this._tickInterval = tickInterval;
		this.lifeGeneration = null;
		this.cellSize = null;
		this.stage = new createjs.Stage("canvas");

		/**
		 * Extra stage to draw grid on it to achieve better perfomance
		 */
		this.gridStage = new createjs.Stage("grid");
		this.calculateCanvasDimension();
		/**
		 * Array to store cells state on temporary generation
		 */
		this.appState = [];
	}

	get tickInterval(){
        return this._tickInterval;
    }

 	set tickInterval(value){
        if(value) 
            this._tickInterval = value;
    }

	calculateCanvasDimension(){
		let canvasBox = document.getElementsByClassName("js-canvas-box")[0];
		let computedStyles = getComputedStyle(canvasBox);
		let canvasBoxWidth = canvasBox.clientWidth;   
		canvasBoxWidth -= parseFloat(computedStyles.paddingLeft) + parseFloat(computedStyles.paddingRight);
		this.cellSize = Math.floor(canvasBoxWidth / this.cellsCount);
		let canvasDimension = Math.round(this.cellsCount * this.cellSize) + 1;
		/**
		  * Adjust stages
		  */
		this.adjustStage(this.stage, canvasDimension);
		this.adjustStage(this.gridStage, canvasDimension);
	}

	adjustStage(stage, canvasDimension){
		stage.canvas.width = stage.canvas.height = canvasDimension;
		stage.setTransform(0.5, 0.5);
	}

	drawGrid(){
		let strokeColor = "#dddddd";
		for (let i = 0; i <= this.cellsCount; i++) {
		  for (let j = 0; j <= this.cellsCount; j++) {
		    let x = j * this.cellSize;
		    let y = i * this.cellSize;

	        let verticalFrame = new createjs.Shape();
	        this.gridStage.addChild(verticalFrame);
	        verticalFrame.graphics.setStrokeStyle(0.5).beginStroke(strokeColor);
	        verticalFrame.graphics.moveTo(y, 0);
	        verticalFrame.graphics.lineTo(y, x);
	        verticalFrame.graphics.endStroke();

	        let horizontalFrame = new createjs.Shape();
	        this.gridStage.addChild(horizontalFrame);
	        horizontalFrame.graphics.setStrokeStyle(0.5).beginStroke(strokeColor);
	        horizontalFrame.graphics.moveTo(0, x);
	        horizontalFrame.graphics.lineTo(y, x);
	        horizontalFrame.graphics.endStroke();
		  }
		}
		this.gridStage.update();
	}

	initGameStates(){
		let tempState = [];
		for (let i = 0; i < this.cellsCount; i++){
			tempState.push([]);
			for(let j=0; j < this.cellsCount; j++){
				tempState[i][j] = false
			}
		}
		this.appState = tempState;
	}

	populateRandom(){
		for (let i = 0; i < this.cellsCount; i++){
			for(let j=0; j < this.cellsCount; j++){
				this.appState[i][j] = Math.floor(Math.random() * 2);
			}
		}
		this.render();
	}

	loadPattern(patternName){
		let pattern = patterns[patternName];
		pattern.map(item => {
			this.appState[parseInt(item.cords.split(",")[0])]
						 [parseInt(item.cords.split(",")[1])] = true;
		});
		this.render();
	}

	render(){
		this.stage.removeAllChildren();
		for (let i = 0; i < this.cellsCount; i++) {
		  for (let j = 0; j < this.cellsCount; j++) {
		    let x = j * this.cellSize;
		    let y = i * this.cellSize;
		    let cell = this.appState[i][j];
		    if (cell){
			    let graphics = new createjs.Graphics().beginFill("#000000").drawRect(x, y, this.cellSize, this.cellSize);	
				let rect = new createjs.Shape(graphics);
				this.stage.addChild(rect);
		    }
		  }
		}
		this.stage.update();
	}

	cellClickHandler(){
		/**
		 * Track clicking on cell
		 */
		this.stage.on("stagemousedown", evt => {
			/**
			 * Calculate game cell based on event coordinates
			 */
			let clickedCellX = Math.floor(evt.stageX / this.cellSize);
			let clickedCellY = Math.floor(evt.stageY / this.cellSize);
			this.appState[clickedCellY][clickedCellX] = this.appState[clickedCellY][clickedCellX] ? false : true;
			this.render();
		});
	}

	tick(){
		if (!createjs.Ticker.getPaused()) {
	 		this.appState = this.lifeGeneration.run();
			this.render();
		}
	}

	isAppRunning(){
		return this.appStatus;
	}

	attachTickIntervalToTicker(tickInterval){
		createjs.Ticker.reset();
		createjs.Ticker.addEventListener("tick", this.tick.bind(this));
		createjs.Ticker.setInterval(tickInterval);
	}

	initTick(){
		this.appStatus = 1;
		this.lifeGeneration = new LifeGeneration(this.appState);
		createjs.Ticker.addEventListener("tick", this.tick.bind(this));
		createjs.Ticker.setInterval(this.tickInterval);
	}

	pauseTick(){
		let paused = !createjs.Ticker.getPaused();
		createjs.Ticker.setPaused(paused);
	}

	clearTick(){
		this.appStatus = 0;
		this.stage.removeAllChildren();
		this.appState = [];
		this.initGameStates();
		createjs.Ticker.reset();
		this.stage.update();
	}

	init(){
		this.drawGrid();
		this.initGameStates();
		this.cellClickHandler();
		let controls = new Controls(this);
	}
}

window.onload = function() {
	let app = new App(200, 0);
	app.init();
};


